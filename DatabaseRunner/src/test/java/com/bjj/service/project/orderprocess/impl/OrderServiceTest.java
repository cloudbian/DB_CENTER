package com.bjj.service.project.orderprocess.impl;

import com.bjj.service.core.OrderStatus;
import com.bjj.service.domain.Msg;
import com.bjj.service.project.orderprocess.domain.QueryOrderParams;
import com.bjj.service.project.webfront.db.HotelOrderRepository;
import com.bjj.service.project.webfront.domain.ChangeExtraService;
import com.bjj.service.project.webfront.domain.ReqOrder;
import com.bjj.service.project.webfront.domain.ReqOrder_Contact;
import com.bjj.service.project.webfront.impl.HotelOrderService;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.lang.reflect.Field;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring_config.xml")
public class OrderServiceTest {


    @Autowired
    private OrderPImpl hotelOrderService;
    
    @Autowired
    private HotelOrderService hotelOrderService2;

    @Test
    public void testSaveReqOrder() throws Exception {
        System.out.println(hotelOrderService);
        QueryOrderParams params = new QueryOrderParams();
        params.setSkip(1);
        JSONObject jsonObject = hotelOrderService.queryOrder(params);
        System.out.println(jsonObject);

        params.setSkip(10);
         jsonObject = hotelOrderService.queryOrder(params);
        System.out.println(jsonObject);


    }

    @Test
    public void testSaveReqOrder2() throws Exception {
        System.out.println(hotelOrderService);
        QueryOrderParams params = new QueryOrderParams();
        params.setStatus(OrderStatus.XJZ.getEnName());
        JSONObject jsonObject = hotelOrderService.queryOrder(params);
        System.out.println(jsonObject);
    }
    @Test
    public void testSaveReqOrder4() throws Exception {
        JSONObject jsonObject = hotelOrderService.orderInfo("791428912878854",null);
        System.out.println(jsonObject);
    }

    @Test
    public void testSaveReqOrder3() throws Exception {
        System.out.println(hotelOrderService);
//        JSONObject jsonObject = hotelOrderService.orderDetail("10000024");
        JSONObject jsonObject = hotelOrderService.orderDetail("061428370466339");
        System.out.println(jsonObject);
    }
    @Test
    public void testOrderLog() throws Exception {
        System.out.println(hotelOrderService);
//        JSONObject jsonObject = hotelOrderService.orderDetail("10000024");
        JSONObject jsonObject = hotelOrderService.orderLogs("10000022");
        System.out.println(jsonObject);
    }

    @Test
    public void testUpdateReqOrder() throws Exception {
        System.out.println(hotelOrderService);
//        JSONObject jsonObject = hotelOrderService.orderDetail("10000024");
        JSONObject pa = new JSONObject();
        pa.put("pk", "54f531f3c22ec8f623e2cad6");

        JSONArray arr = new JSONArray();
        JSONObject u_one = new JSONObject();
        u_one.put("name", "remark");
        u_one.put("name_cn", "备注 ");
        u_one.put("value", "sdsddsxdxx");
        arr.add(u_one);

        JSONObject u_two = new JSONObject();
        u_two.put("name", "status");
        u_two.put("value", "XJZ");
        arr.add(u_two);

        JSONObject u_3 = new JSONObject();
        u_3.put("name", "totalPrice");
        u_3.put("value", 2323);
        arr.add(u_3);

        pa.put("update", arr);

        JSONObject jsonObject = hotelOrderService.updateOrder(pa);
        System.out.println(jsonObject);


    }


    @Test
    public void testReflect() throws Exception{
        ReqOrder one = new ReqOrder();

        Class cls = one.getClass();
        Field field = cls.getDeclaredField("roomNumber");
        field.setAccessible(true);
        System.out.println(field);
        Class<?> fieldType = field.getType();
        System.out.println(fieldType);
        Object o = field.get(one);
        System.out.println(o);

    }
    
    
    @Test
    public void testChangeExtraService() throws Exception{
    	ChangeExtraService changeExtraService = new ChangeExtraService();
        String str = "[{'date':'1430064000000','price':'10','type':'温泉票'},{'date':'1430150400000','price':'10','type':'温泉票'},{'date':'1430064000000','price':'5','type':'早餐'},{'date':'1430150400000','price':'5','type':'早餐'}]";
        changeExtraService.setOrderId("381428458926804");
        changeExtraService.setExtraServicePriceArray(str);
        hotelOrderService2.changeExtraService(changeExtraService);

    }
    
    
    
}