package com.bjj.service.project.orderprocess.impl;

import com.bjj.service.core.OrderStatus;
import com.bjj.service.project.orderprocess.domain.QueryOrderParams;
import com.bjj.service.project.webfront.domain.ReqOrder;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.lang.reflect.Field;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring_config.xml")
public class AdminTest {


    @Autowired
    private AdminImpl hotelOrderService;

    @Test
    public void tesQueryOrder() throws Exception {
        System.out.println(hotelOrderService);


        JSONObject p = new JSONObject();
        p.put("user", "admin");
//        p.put("pwd", "aaa");
        p.put("pwd", "1234");
        JSONObject jsonObject = hotelOrderService.doLogin(p);
        System.out.println(jsonObject);

    }
    @Test
    public void testSaveReqOrder() throws Exception {
        System.out.println(hotelOrderService);

        JSONObject p = new JSONObject();
        p.put("user", "admin3");
        p.put("pwd", "1234");
        p.put("mobile", "139123456555");
        JSONObject jsonObject = hotelOrderService.save(p);
        System.out.println(jsonObject);

    }

}