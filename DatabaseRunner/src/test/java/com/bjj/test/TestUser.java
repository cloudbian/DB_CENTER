/**
 * 
 */
package com.bjj.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bjj.service.exception.ServiceException;
import com.bjj.service.project.webfront.db.UserRepository;
import com.bjj.service.project.webfront.impl.UserService;
import com.bjj.service.utils.MongoDB;

/**
 * @author June
 * CreateTime：2015-3-23  下午4:25:44
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring_config.xml")
public class TestUser {

	@Autowired
	UserService user;
	
	@Autowired
	UserRepository repo;
	
	@Test
	public void Test(){
//		User temp = user.doLogin("bjj", "123123");
//		System.out.println(temp);
		com.bjj.service.model.webfront.User user = new com.bjj.service.model.webfront.User();
		user.setMobile("15211111111");
		user.setEmail("gonglinhu@bingdian.com");
		user.setName("name");
		user.setPassWord("5555555555555");
		user.setUserName("15211111111");
		repo.save(user);
	}
	
	@Test
	public void TestUpdate() throws ServiceException{
//		user.useInviteCode("13811111111", "2BBW8K");
		user.valiCode("13816040145", "5712");
	}
	
	@Test
	public void TestSave(){
//		user.saveInviteCode("15298836119", "ABCNMD");
		try {
			user.useInviteCode("15618320177", "ABCNMD");
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
}
