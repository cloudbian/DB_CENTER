package com.bjj.service.core;

import com.bjj.service.model.OrderIDS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

/**
 * @author Jiangli
 *         <p/>
 *         CreatedTime  2015/3/25 0025 15:32
 */
@Component
public class OrderIdGen {
    @Autowired
    private MongoOperations mongoOperationNew;

    private int initOrderId = 10000000;

    public String generateNewOrderId() {
        int r1 = (int) (Math.random() * (10));//产生2个0-9的随机数
        int r2 = (int) (Math.random() * (10));
        long now = System.currentTimeMillis();//一个13位的时间戳
        String paymentID = String.valueOf(r1) + String.valueOf(r2) + String.valueOf(now);// 订单ID

        return paymentID;
    }

    public String _v1_generateNewOrderId() {
        Query query = new Query();
        query.with(new Sort(new Sort.Order(Sort.Direction.DESC, "oId")));

        OrderIDS one = mongoOperationNew.findOne(query, OrderIDS.class);
        OrderIDS next = null;
        if (one == null) {
            one = new OrderIDS();
            one.setoId(initOrderId);

            next = one;
        } else {
            next = new OrderIDS();
            next.setoId(one.getoId() + 1);
        }

        mongoOperationNew.save(next);
        return String.valueOf(next.getoId());
    }
}
