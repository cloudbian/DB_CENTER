package com.bjj.service.core;

import com.bjj.service.project.orderprocess.impl.OrderPImpl;
import net.sf.json.JSONObject;
import org.springframework.data.mongodb.core.query.Update;

/**
 * @author Jiangli
 *         <p/>
 *         CreatedTime  2015/4/3 0003 10:13
 */
public interface PropChangeInterface<T> {
    JSONObject beforeChange(Object oldVal, Object newValue, T one, Update update, JSONObject upparams) throws OrderPImpl.UpdateInterruptException;
}
