package com.bjj.service.core;

/**
 * @author Jiangli
 *         <p/>
 *         CreatedTime  2015/3/23 0023 11:10
 */
public enum OrderStatus {
    XJZ("XJZ", "询价中"),
    DZF("DZF", "待支付"),
    DQR("DQR", "待确认"),
    YQR("YQR", "已确认"),
    YQX("YQX", "已取消"),
    YGB("YGB", "已关闭"),
    UNKNOWN("UNKNOWN", "未知状态");


    OrderStatus(String enName, String cnName) {
        this.enName = enName;
        this.cnName = cnName;
    }

    private String enName;
    private String cnName;

    public String getEnName() {
        return enName;
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }

    public String getCnName() {
        return cnName;
    }

    public void setCnName(String cnName) {
        this.cnName = cnName;
    }

    public static OrderStatus get(String status) {
        for (OrderStatus orderStatus : values()) {
            if (orderStatus.getEnName().equals(status)) {
                return orderStatus;
            }
        }
        return UNKNOWN;
    }


}
