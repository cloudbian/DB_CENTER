package com.bjj.service.utils;

import org.apache.commons.lang.StringUtils;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author Jiangli
 *         <p/>
 *         CreatedTime  2015/3/11 0011 16:23
 */
public class CommonUtil {
    /**
     * 获得Object的字符串
     *
     * @param obj
     * @return String
     * @author JiangLi CreateTime 2014-3-11 下午5:43:36
     */
    public static String nullToEmpty(Object obj) {
        return obj == null ? "" : obj.toString();
    }

    public static String getDateDiff(Long d1, Long d2) {
        if (d1 != null && d2 != null) {
            long l = Math.abs(d1 - d2) / TimeUnit.DAYS.toMillis(1);
            return String.valueOf(l);
        }
        return "";
    }

    public static String formatOrderPrice(Double d) {
        return formatOrderPrice(d, "");
    }

    public static String formatOrderPrice(Double d, String defaultWhenNull) {
        if (d == null) {
            return defaultWhenNull;
        }
        return new BigDecimal(d).setScale(0, BigDecimal.ROUND_FLOOR).toString();
    }

    public static boolean isStringNull(String obj) {
        return StringUtils.isEmpty(obj);
    }

    public static boolean isStringNotNull(String obj) {
        return !isStringNull(obj);
    }

    public static <T> T findOne(Class<T> cls, String prop, Object value) {
        return MongoDB.getMongoDB().findOne(Query.query(Criteria.where(prop).is(value)), cls);
    }

    public static <T> T findOneByObjectId(Class<T> cls, Object value) {
        if (value != null) {
            return MongoDB.getMongoDB().findOne(Query.query(Criteria.where("_id").is(value)), cls);
        } else {
            return null;
        }
    }

    public static <T> List<T> findList(Class<T> cls, String prop, Object value) {
        return MongoDB.getMongoDB().find(Query.query(Criteria.where(prop).is(value)), cls);
    }

    public static boolean isObjectId(String obj) {
        if (isStringNull(obj)) {
            return false;
        }
        return obj.length() == 24;
    }
}
