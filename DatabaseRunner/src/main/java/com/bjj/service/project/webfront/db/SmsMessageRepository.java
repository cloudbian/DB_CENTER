/**
 *
 */
package com.bjj.service.project.webfront.db;

import com.bjj.service.model.webfront.SmsMessage;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

/**
 * @author June
 *         CreateTime：2015-3-23  下午5:08:09
 */
public interface SmsMessageRepository extends MongoRepository<SmsMessage, ObjectId> {
    @Query("{'mobile':?0}")
    public SmsMessage findSmsMessage(String mobile, Sort sort);

    @Override
    public <S extends SmsMessage> S save(S arg0);
}
