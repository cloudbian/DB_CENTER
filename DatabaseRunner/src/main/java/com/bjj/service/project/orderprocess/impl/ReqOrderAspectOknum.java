package com.bjj.service.project.orderprocess.impl;

import com.bjj.service.core.OrderStatus;
import com.bjj.service.model.webfront.ReqOrder;
import com.mongodb.DBObject;
import net.sf.json.JSONObject;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

/**
 * @author Jiangli
 *         <p/>
 *         CreatedTime  2015/4/3 0003 10:29
 */
@Component
public class ReqOrderAspectOknum extends ReqOrderAspect {
    public ReqOrderAspectOknum() {
        super("okNum");
    }

    @Override
    public JSONObject beforeChange(Object oldVal, Object newValue, ReqOrder one, Update update, JSONObject upparams) throws OrderPImpl.UpdateInterruptException {
        DBObject updateObject = update.getUpdateObject();
        if(updateObject.containsField("status")){
            throw new OrderPImpl.UpdateInterruptException(400000, "修改确认号时不能同时修改订单状态");
        }

        if (OrderStatus.get(one.getStatus()) == OrderStatus.DQR) {
            update.set("status", OrderStatus.YQR.getEnName());
        } else {
            throw new OrderPImpl.UpdateInterruptException(400001, "无法为订单状态为"+OrderStatus.get(one.getStatus()).getCnName()+"的订单修改确认号");
        }

        JSONObject ret = new JSONObject();
        return ret;
    }
}
