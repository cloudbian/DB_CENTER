package com.bjj.service.project.orderprocess.inf;


import net.sf.json.JSONObject;


public interface IAdminService {

    /**
     * @param params
     * @return
     */
    JSONObject doLogin(JSONObject params);

    JSONObject save(JSONObject params);

}
