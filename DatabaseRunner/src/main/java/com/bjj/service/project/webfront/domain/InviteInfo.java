/**
 * 
 */
package com.bjj.service.project.webfront.domain;

import java.io.Serializable;

import org.springframework.data.annotation.PersistenceConstructor;

/**
 * @author June
 * CreateTime：2015-4-10  下午3:15:00
 */
public class InviteInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String inviteCode;
	private String num;

	public String getInviteCode() {
		return inviteCode;
	}

	public void setInviteCode(String inviteCode) {
		this.inviteCode = inviteCode;
	}

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	/**
	 * @param inviteCode
	 * @param num
	 */
	@PersistenceConstructor
	public InviteInfo(String inviteCode, String num) {
		super();
		this.inviteCode = inviteCode;
		this.num = num;
	}

	/**
	 * 
	 */
	public InviteInfo() {
		super();
	}

	@Override
	public String toString() {
		return "InviteInfo [inviteCode=" + inviteCode + ", num=" + num + "]";
	}
	
}
