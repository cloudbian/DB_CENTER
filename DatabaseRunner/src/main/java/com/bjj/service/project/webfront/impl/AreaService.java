package com.bjj.service.project.webfront.impl;

import com.bjj.service.core.RMIServiceExporterAnno;
import com.bjj.service.project.webfront.db.AreaRepository;
import com.bjj.service.project.webfront.domain.Area;
import com.bjj.service.project.webfront.inf.IArea;
import com.bjj.service.utils.BeanCopyUtil;
import com.bjj.tools.LogUtil;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RMIServiceExporterAnno(serviceInterface = IArea.class)
public class AreaService implements IArea {
    private LogUtil log = LogUtil.getInstance("webFront");

    @Autowired
    AreaRepository repo;

    public List<Area> getAllAreas(String cityId) {
        return BeanCopyUtil.getCopyBeansFromObjecIdToStr(repo.findAreasByCityId(new ObjectId(cityId)), Area.class);
    }
}
