package com.bjj.service.project.orderprocess.domain;

import java.io.Serializable;

/**
 * @author Jiangli
 *         <p/>
 *         CreatedTime  2015/3/25 0025 12:25
 */
public class QueryOrderParams implements Serializable {


    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer skip = 0;
    private Integer pageSize = 10;
    private String orderNum;
    private Boolean orderNumReg = true;
    private String orderDate;
    private String startDate;
    private String endDate;
    private String propName;
    private Boolean propNameReg = true;
    private String status;
    private String contactName;
    private Boolean contactNameReg = true;
    private String contactPhone;
    private Boolean contactPhoneReg = true;
    private String userId;

    public Integer getSkip() {
        return skip;
    }

    public Boolean getOrderNumReg() {
        return orderNumReg;
    }

    public void setOrderNumReg(Boolean orderNumReg) {
        this.orderNumReg = orderNumReg;
    }

    public Boolean getPropNameReg() {
        return propNameReg;
    }

    public void setPropNameReg(Boolean propNameReg) {
        this.propNameReg = propNameReg;
    }

    public Boolean getContactNameReg() {
        return contactNameReg;
    }

    public void setContactNameReg(Boolean contactNameReg) {
        this.contactNameReg = contactNameReg;
    }

    public Boolean getContactPhoneReg() {
        return contactPhoneReg;
    }

    public void setContactPhoneReg(Boolean contactPhoneReg) {
        this.contactPhoneReg = contactPhoneReg;
    }

    public void setSkip(Integer skip) {
        this.skip = skip;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getPropName() {
        return propName;
    }

    public void setPropName(String propName) {
        this.propName = propName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }
    
    

    public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
    public String toString() {
        return "QueryOrderParams{" +
                "skip=" + skip +
                ", pageSize=" + pageSize +
                ", orderNum='" + orderNum + '\'' +
                ", orderNumReg=" + orderNumReg +
                ", orderDate='" + orderDate + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", propName='" + propName + '\'' +
                ", propNameReg=" + propNameReg +
                ", status='" + status + '\'' +
                ", contactName='" + contactName + '\'' +
                ", contactNameReg=" + contactNameReg +
                ", contactPhone='" + contactPhone + '\'' +
                ", contactPhoneReg=" + contactPhoneReg +
                ", userId=" + userId +
                '}';
    }
}
