package com.bjj.service.project.webfront.impl;

import com.bjj.service.core.RMIServiceExporterAnno;
import com.bjj.service.project.webfront.db.CityRepository;
import com.bjj.service.project.webfront.domain.City;
import com.bjj.service.project.webfront.inf.ICityService;
import com.bjj.service.utils.BeanCopyUtil;
import com.bjj.tools.LogUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RMIServiceExporterAnno(serviceInterface = ICityService.class)
public class CityService implements ICityService {
    private LogUtil log = LogUtil.getInstance("webFront");

    @Autowired
    CityRepository repo;

    @Override
    public List<City> getAllCitys() {
        return BeanCopyUtil.getCopyBeansFromObjecIdToStr(repo.findAll(), City.class);
    }

    @Override
    public List<City> getCitysLikeName(String name) {
        return BeanCopyUtil.getCopyBeansFromObjecIdToStr(repo.findCitysLikeName(name), City.class);
    }
}
