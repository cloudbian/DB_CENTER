package com.bjj.service.project.orderprocess.impl;

import com.bjj.service.core.OrderStatus;
import com.bjj.service.model.webfront.ReqOrder;
import com.bjj.service.utils.CommonUtil;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Jiangli
 *         <p/>
 *         CreatedTime  2015/4/3 0003 10:29
 */
@Component
public class ReqOrderAspectStatus extends ReqOrderAspect {
    boolean restrict = true;//设为true时找不到对应的StatusChangeDetail将中端更新。

    @Autowired
    List<StatusChangeDetail> logics;

    public ReqOrderAspectStatus() {
        super("status");
    }

    @Override
    public JSONObject beforeChange(Object oldVal, Object newValue, ReqOrder one, Update update, JSONObject upparams) throws OrderPImpl.UpdateInterruptException {
        JSONObject ret = new JSONObject();


        OrderStatus newStatus = OrderStatus.get(CommonUtil.nullToEmpty(newValue));
        OrderStatus oldStatus = OrderStatus.get(CommonUtil.nullToEmpty(oldVal));
        for (StatusChangeDetail logic : logics) {
            if (logic.getOldVal() == oldStatus && logic.getNewValue() == newStatus) {
                logic.beforeChange(one, update);
                return ret;
            }
        }

        if (restrict) {
            throw new OrderPImpl.UpdateInterruptException(300000, "状态不能从(" + oldStatus.getCnName() + ")直接变为(" + newStatus.getCnName() + ")");
        }

        return ret;
    }
}
