package com.bjj.service.project.webfront.db;

import com.bjj.service.model.webfront.City;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface CityRepository extends MongoRepository<City, ObjectId> {
    @Override
    public List<City> findAll();

    @Query("{$or:[{'name':{'$regex':?0}},{'eName':{'$regex':?0}}]}")
    public List<City> findCitysLikeName(String name);
}
