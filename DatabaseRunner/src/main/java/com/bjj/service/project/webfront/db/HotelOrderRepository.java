package com.bjj.service.project.webfront.db;

import com.bjj.service.model.webfront.ReqOrder;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface HotelOrderRepository extends MongoRepository<ReqOrder, ObjectId> {
    @Override
    public <S extends ReqOrder> S save(S arg0);

    @Query("{}")
    public List<ReqOrder> queryAllOrders();
}
