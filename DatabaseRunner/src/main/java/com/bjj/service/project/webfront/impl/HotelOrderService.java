package com.bjj.service.project.webfront.impl;

import com.bjj.service.core.OrderIdGen;
import com.bjj.service.core.OrderStatus;
import com.bjj.service.core.RMIServiceExporterAnno;
import com.bjj.service.domain.Msg;
import com.bjj.service.exception.MsgException;
import com.bjj.service.model.webfront.ExtraService;
import com.bjj.service.model.webfront.Property;
import com.bjj.service.model.webfront.ReqOrderLog;
import com.bjj.service.model.webfront.UnitPrice;
import com.bjj.service.project.webfront.db.*;
import com.bjj.service.project.webfront.domain.ChangeExtraService;
import com.bjj.service.project.webfront.domain.ReqOrder;
import com.bjj.service.project.webfront.domain.UpdOrderStatus;
import com.bjj.service.project.webfront.inf.IHotelOrderService;
import com.bjj.service.utils.BeanCopyUtil;
import com.bjj.service.utils.CommonUtil;
import com.bjj.service.utils.DateUtils;
import com.bjj.tools.LogUtil;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


@Component
@RMIServiceExporterAnno(serviceInterface = IHotelOrderService.class)
public class HotelOrderService implements IHotelOrderService {
    @Autowired
    HotelOrderRepository repo;
    @Autowired
    CityRepository cityRepo;
    @Autowired
    AreaRepository areaRepo;
    @Autowired
    HotelRepository hotelRepo;
    @Autowired
    UserRepository userRepository;

    @Autowired
    OrderIdGen orderIdGen;

    @Autowired
    DateUtils dateUtils;

    @Autowired
    private MongoOperations mongoOperationNew;

    private LogUtil log = LogUtil.getInstance("webFront");

    @Override
    public Msg<ReqOrder> saveReqOrder(ReqOrder rOrder) {
        Msg<ReqOrder> ret = new Msg<ReqOrder>();
        try {
            if (CommonUtil.isStringNull(rOrder.getHotelId())) {
                rOrder.setHotelName(rOrder.getCustomName());
            }

            if (CommonUtil.isStringNotNull(rOrder.getHotelId())) {
                ret.throwIfStringNotObjectIdException(rOrder.getHotelId(), 2, "hotelId不是ObjectId类型");

                Property hotelOne = CommonUtil.findOneByObjectId(Property.class, new ObjectId(rOrder.getHotelId()));
                ret.throwIfNullException(hotelOne, 3, "找不到hotelId对应的数据");
                rOrder.setHotelName(hotelOne.getName());
            }

            String userId = rOrder.getUserId();
            ret.throwIfNullException(userId, 3, "userId不能为空");
            ret.throwIfStringNotObjectIdException(userId, 4, "userId不是ObjectId类型");

            //save
            String contactMobile = rOrder.getContactMobile();
            String contactEmail = rOrder.getContactEmail();
            String contactName = rOrder.getContactName();

            com.bjj.service.model.webfront.ReqOrder dbModel = new com.bjj.service.model.webfront.ReqOrder();
            BeanCopyUtil.getCopyBeanFromStrToObjectId(rOrder, dbModel);

            dbModel.setCreateTime(System.currentTimeMillis());
            dbModel.setUserId(new ObjectId(userId));
            dbModel.setStatus(OrderStatus.XJZ.getEnName());
            dbModel.setOrderId(orderIdGen.generateNewOrderId());
            dbModel.setBedTypeName(rOrder.getBedType());
            dbModel.setContactPhone(contactMobile);
            repo.save(dbModel);


            BeanCopyUtil.getCopyBeanFromObjecIdToStr(dbModel, rOrder);
            ret.setData(rOrder);//success

        } catch (MsgException e) {

        } catch (Exception e) {
            log.logError(e.getMessage(), e);
            ret.error(1, "未知异常", e.getClass() + "\t" + e.getMessage());
        }
        return ret;
    }

    @Override
    public Msg<UpdOrderStatus> updOrderStatus(UpdOrderStatus updOrderStatus) {
        // TODO Auto-generated method stub
        Msg<UpdOrderStatus> ret = new Msg<UpdOrderStatus>();
        Query query = new Query();
        Update updateDB = new Update();
        query.addCriteria(Criteria.where("orderId").is(updOrderStatus.getOrderId()));
        com.bjj.service.model.webfront.ReqOrder orderOne = mongoOperationNew.findOne(query, com.bjj.service.model.webfront.ReqOrder.class);
        updateDB.set("status", updOrderStatus.getStatus());
        if (orderOne != null) {
            mongoOperationNew.updateFirst(query, updateDB, com.bjj.service.model.webfront.ReqOrder.class);
            String operContent = "";
            operContent += "订单状态由" + OrderStatus.get(orderOne.getStatus()).getCnName() + "更改为" + OrderStatus.get(updOrderStatus.getStatus()).getCnName();
            //记日志
            ReqOrderLog log = new ReqOrderLog();
            log.setContent(operContent);
            log.setOperTime(System.currentTimeMillis());
            log.setOrderId(updOrderStatus.getOrderId());
            log.setOperAdminMobile(updOrderStatus.getMobile());
            updOrderStatus.setPropName(orderOne.getHotelName());
	        ret.setData(updOrderStatus);
            mongoOperationNew.save(log);
        } else {
            ret.setCode(1);
            ret.setMsg("订单号" + updOrderStatus.getOrderId() + "不存在");
        }

        return ret;
    }

    @Override
    public void changeExtraService(ChangeExtraService changeExtraService) {
        // TODO Auto-generated method stub
        JSONArray extraServicePriceArray = JSONArray.fromObject(changeExtraService.getExtraServicePriceArray());
        Query query = new Query();
        Update updateDB = new Update();
        query.addCriteria(Criteria.where("orderId").is(changeExtraService.getOrderId()));
        com.bjj.service.model.webfront.ReqOrder orderOne = mongoOperationNew.findOne(query, com.bjj.service.model.webfront.ReqOrder.class);
        if (orderOne != null) {
            double extraServicePriceCount = 0;
            List<UnitPrice> units = orderOne.getUnit();
            for (int i = 0; i < units.size(); i++) {
                UnitPrice unitOne = units.get(i);
                double totalPrice = unitOne.getTotalPrice();
                List<ExtraService> extraServices = new ArrayList<ExtraService>();
                for (int j = 0; j < extraServicePriceArray.size(); j++) {
                    JSONObject extraServicePriceOne = extraServicePriceArray.getJSONObject(j);
                    if (extraServicePriceOne.getLong("date") == unitOne.getDate()) {
                        ExtraService extraServiceOne = new ExtraService();
                        extraServiceOne.setType(extraServicePriceOne.getString("type"));
                        extraServiceOne.setInputPrice(extraServicePriceOne.getDouble("price"));
                        totalPrice += extraServicePriceOne.getDouble("price");
                        extraServicePriceCount += extraServicePriceOne.getDouble("price");
                        extraServices.add(extraServiceOne);
                    }
                }
                unitOne.setTotalPrice(totalPrice);
                if (extraServices.size() > 0) {
                    unitOne.setExtraService(extraServices);
                }
            }


            updateDB.set("unit", units);
            updateDB.set("totalPrice", orderOne.getTotalPrice() + extraServicePriceCount);
            mongoOperationNew.updateFirst(query, updateDB, com.bjj.service.model.webfront.ReqOrder.class);
            String operContent = "";
            operContent += "订单新增服务价格,费用总计" + extraServicePriceCount + "元";
            //记日志
            ReqOrderLog log = new ReqOrderLog();
            log.setContent(operContent);
            log.setOperTime(System.currentTimeMillis());
            log.setOrderId(changeExtraService.getOrderId());
            mongoOperationNew.save(log);
        }

    }


}
