package com.bjj.service.project.webfront.domain;

import java.io.Serializable;

public class ChangeExtraService implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -5297968621161872704L;
    private String orderId;
    private String extraServicePriceArray;
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getExtraServicePriceArray() {
		return extraServicePriceArray;
	}
	public void setExtraServicePriceArray(String extraServicePriceArray) {
		this.extraServicePriceArray = extraServicePriceArray;
	}
    
    
    
    
}
