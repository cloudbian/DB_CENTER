package com.bjj.service.project.orderprocess.inf;

import com.bjj.service.project.orderprocess.domain.QueryOrderParams;
import net.sf.json.JSONObject;

/**
 * @author Jiangli
 *         <p/>
 *         CreatedTime  2015/3/25 0025 12:22
 */
public interface IOrderPInf {
    JSONObject queryOrder(QueryOrderParams params);

    JSONObject orderLogs(String orderId);

    JSONObject orderDetail(String orderId);

    JSONObject orderInfo(String orderId, String degree);

    /**
     * req:{
     * pk:
     * ~ remarkSplit:default <br/>
     * ~remark:
     * operId:
     * operIp:
     * update:[{
     * name:
     * ~   name_cn :default name
     * ~ remark:
     * value:
     * }
     * ]
     * }
     * <p/>
     * <p/>
     * res:{
     * code:
     * msg:
     * results:[{
     * key:
     * code:
     * msg:
     * }
     * ]
     * }
     *
     * @return
     */
    JSONObject updateOrder(JSONObject param);

}
