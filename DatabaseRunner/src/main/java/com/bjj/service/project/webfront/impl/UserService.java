package com.bjj.service.project.webfront.impl;

import com.bjj.service.core.RMIServiceExporterAnno;
import com.bjj.service.exception.ServiceException;
import com.bjj.service.model.webfront.InviteCode;
import com.bjj.service.model.webfront.SmsMessage;
import com.bjj.service.project.webfront.db.InviteCodeRepository;
import com.bjj.service.project.webfront.db.SmsMessageRepository;
import com.bjj.service.project.webfront.db.UserRepository;
import com.bjj.service.project.webfront.domain.InviteInfo;
import com.bjj.service.project.webfront.domain.User;
import com.bjj.service.project.webfront.inf.IUserService;
import com.bjj.service.utils.BeanCopyUtil;
import com.bjj.service.utils.MongoDB;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.List;

@Component
@RMIServiceExporterAnno(serviceInterface = IUserService.class)
public class UserService implements IUserService {
    protected static final Log log = LogFactory.getLog("user");

    @Autowired
    UserRepository repo;

    @Autowired
    SmsMessageRepository msgRepo;

    @Autowired
    InviteCodeRepository icRepo;

    @Override
    public User doLogin(String userName, String passWord) {
        return BeanCopyUtil.getCopyBeanFromObjecIdToStr(repo.findUserByNameAndPwd(userName, passWord), new User());
    }

    @Override
    public User getUserMobile(String userName) {
        return BeanCopyUtil.getCopyBeanFromObjecIdToStr(repo.findMobileByName(userName), new User());
    }

    @Override
    public List<User> getAllUsers() {
        return BeanCopyUtil.getCopyBeansFromObjecIdToStr(repo.findAll(), User.class);
    }

    @Override
    public void valiCode(String mobile, String code) throws ServiceException {
        SmsMessage msg = msgRepo.findSmsMessage(mobile, new Sort(Sort.Direction.DESC, "validate"));
        if (msg != null) {
            if (msg.getValidate() >= Calendar.getInstance().getTimeInMillis()) {
                if (code.equals(msg.getCode())) {
                } else {
                    log.info("输入的验证码不正确！输入的是" + code + ",应该是" + msg.getCode());
                    throw new ServiceException("输入的验证码不正确！");
                }
            } else {
                log.info("验证码已失效！");
                throw new ServiceException("验证码已失效！");
            }
        } else {
            log.info("输入的验证码不正确,没有此手机号:(" + mobile + ")所对应的验证码");
            throw new ServiceException("输入的验证码不正确！");
        }
    }

    /* (non-Javadoc)
     * @see com.bjj.service.project.webfront.inf.IUserService#saveSmsMessage(java.lang.String, java.lang.String)
     */
    @Override
    public void saveSmsMessage(String code, String mobile) {
        // TODO Auto-generated method stub
        SmsMessage smsMessage = new SmsMessage();
        Calendar now = Calendar.getInstance();
        now.set(Calendar.MINUTE, now.get(Calendar.MINUTE) + 30);
        smsMessage.setValidate(now.getTimeInMillis());
        smsMessage.setCode(code);
        smsMessage.setMobile(mobile);
        msgRepo.save(smsMessage);
    }

    /* (non-Javadoc)
     * @see com.bjj.service.project.webfront.inf.IUserService#saveInviteCode(java.lang.String, java.lang.String)
     */
    @Override
    public void saveInviteCode(String mobile, String inviteCode) {
        // TODO Auto-generated method stub
        InviteCode code = new InviteCode();
        //直接保存该记录
        code.setInviteCode(inviteCode);
        code.setMobile(mobile);//mobile为登录人 就是父值
        code.setNum(0);
        code.setCreateTime(System.currentTimeMillis());
        icRepo.save(code);
    }

    /* (non-Javadoc)
     * @see com.bjj.service.project.webfront.inf.IUserService#saveUser(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public User saveUser(String mobile, String email, String password) throws ServiceException {
        // TODO Auto-generated method stub
        com.bjj.service.model.webfront.User user = repo.findUserByMobile(mobile);
        if (user != null) {
            throw new ServiceException("该手机号已经被注册过了，请直接登录");
        }
        user = new com.bjj.service.model.webfront.User();
        user.setEmail(email);
        user.setMobile(mobile);
        user.setName("");
        user.setUserName(mobile);
        user.setPassWord(password);//md5进行加密
        repo.save(user);
        return BeanCopyUtil.getCopyBeanFromObjecIdToStr(user, new User());
    }

    /* (non-Javadoc)
     * @see com.bjj.service.project.webfront.inf.IUserService#updatePwd(java.lang.String, java.lang.String)
     */
    @Override
    public void updatePwd(String mobile, String password) {
        // TODO Auto-generated method stub
        Query query = new Query();
        query.addCriteria(Criteria.where("mobile").is(mobile));
        Update update = new Update();
        update.set("passWord", password);
        MongoDB.getMongoDB().updateFirst(query, update, com.bjj.service.model.webfront.User.class);
    }

    /* (non-Javadoc)
     * @see com.bjj.service.project.webfront.inf.IUserService#useInviteCode(java.lang.String, java.lang.String)
     */
    @Override
    public void useInviteCode(String mobile, String inviteCode) throws ServiceException {
        // TODO Auto-generated method stub
        InviteCode ic = icRepo.getObj(inviteCode);
        String parentMobile = "";
        if (ic == null) {
            throw new ServiceException("请填写正确的邀请码");
        } else {
            parentMobile = ic.getMobile();
//            if (ic.getNum() == 5) {
//                throw new ServiceException("邀请码已失效");
//            }
            //更新数量
            Query query = new Query();
            query.addCriteria(Criteria.where("mobile").is(parentMobile));
            query.addCriteria(Criteria.where("inviteCode").is(inviteCode));
            Update update = new Update();
            update.set("num", ic.getNum() + 1);
            update.addToSet("childMobile", mobile);
            MongoDB.getMongoDB().updateFirst(query, update, InviteCode.class);
        }
    }

    /* (non-Javadoc)
     * @see com.bjj.service.project.webfront.inf.IUserService#getNum(java.lang.String, java.lang.String)
     */
    @Override
    public InviteInfo getNum(String mobile){
        // TODO Auto-generated method stub
        InviteCode code = icRepo.findNum(mobile, new Sort(Sort.Direction.DESC, "createTime"));
        InviteInfo info = null;
        if (code != null) {
        	info = new InviteInfo();
        	info.setInviteCode(code.getInviteCode());
            info.setNum(new Integer(code.getNum()).toString());
        }
        return info;
    }

	/* (non-Javadoc)
	 * @see com.bjj.service.project.webfront.inf.IUserService#verityLogin(java.lang.String)
	 */
	@Override
	public boolean verityLogin(String adminId) {
		// TODO Auto-generated method stub
		boolean isLogin = false;
		com.bjj.service.model.webfront.User user = repo.findUserById(new ObjectId(adminId));
		if(user!=null){
			isLogin = true;
		}
		return isLogin;
	}
}
