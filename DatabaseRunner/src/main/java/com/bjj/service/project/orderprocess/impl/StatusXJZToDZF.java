package com.bjj.service.project.orderprocess.impl;

import com.bjj.service.core.OrderStatus;
import com.bjj.service.model.orderprocess.DZFOrderIds;
import com.bjj.service.model.webfront.ReqOrder;
import com.bjj.service.utils.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

/**
 * @author Jiangli
 *         <p/>
 *         CreatedTime  2015/4/3 0003 11:00
 */
@Component
public class StatusXJZToDZF extends StatusChangeDetail {
    @Autowired
    private MongoOperations mongoOperationNew;

    public StatusXJZToDZF() {
        super(OrderStatus.XJZ, OrderStatus.DZF);
    }

    @Override
    public void beforeChange(ReqOrder one, Update update) throws OrderPImpl.UpdateInterruptException {
//        System.out.println("StatusXJZToDZF");

        String oId = CommonUtil.nullToEmpty(one.getOrderId());
        DZFOrderIds rec = CommonUtil.findOne(DZFOrderIds.class, "orderId", oId);
        if (rec == null) {
            rec = new DZFOrderIds();
            rec.setU(false);
            rec.setOrderId(oId);
            rec.setTime(System.currentTimeMillis());
            mongoOperationNew.save(rec);
        }
    }
}
