package com.bjj.service.project.webfront.inf;


import com.bjj.service.project.webfront.domain.Area;

import java.util.List;

/**
 * @author Jiangli
 *         <p/>
 *         CreatedTime  2015/3/3 0003 16:58
 */
public interface IArea {
    public List<Area> getAllAreas(String cityId);
}
