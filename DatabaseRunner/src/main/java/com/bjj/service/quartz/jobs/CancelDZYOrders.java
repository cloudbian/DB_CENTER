package com.bjj.service.quartz.jobs;

import com.bjj.service.core.OrderStatus;
import com.bjj.service.model.orderprocess.DZFOrderIds;
import com.bjj.service.model.webfront.ReqOrder;
import com.bjj.service.model.webfront.ReqOrderLog;
import com.bjj.service.quartz.main.Job;
import com.mongodb.WriteResult;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author Jiangli
 *         <p/>
 *         CreatedTime  2015/4/3 0003 14:10
 */
public class CancelDZYOrders implements Job {
//        public final static long cancelTime = TimeUnit.MINUTES.toMillis(1);
    public static final  long cancelTime = TimeUnit.MINUTES.toMillis(30);
    private  final Log logger = LogFactory.getLog("cancelOrderJob");
    public  final OrderStatus E_CANCELLED_STATUS = OrderStatus.YGB;
    public  final OrderStatus E_TO_BE_CANCELLED_STATUS = OrderStatus.DZF;
    public  final String CANCELLED_STATUS = E_CANCELLED_STATUS.getEnName();//已关闭
    public  final String TO_BE_CANCELLED_STATUS = E_TO_BE_CANCELLED_STATUS.getEnName();//待支付

    @Autowired
    private MongoOperations mongoOperationNew;


    @Override
    public void execute() {
        Query query = new Query();
        query.addCriteria(Criteria.where("u").is(false));
        query.addCriteria(Criteria.where("time").lte(System.currentTimeMillis() - cancelTime));
        query.fields().include("orderId");


        try {
            List<DZFOrderIds> dzfOrderIdses = mongoOperationNew.find(query, DZFOrderIds.class);
            List<String> orderIds = new LinkedList<String>();
            for (DZFOrderIds dzfOrderIdse : dzfOrderIdses) {
                orderIds.add(dzfOrderIdse.getOrderId());
            }

            if (orderIds.size() > 0) {
                logger.info("超时订单:" + orderIds);

                /*update DZFOrderIds*/
                markUpdatedDZFOrderIds(orderIds);
                /*update DZFOrderIds*/

                /*实际取消的订单realCancelledOrderIds 数量 <= orderIds数量,因为orderIds可能包含了已取消的订单*/
                List<String> realCancelledOrderIds=cancelOrders(orderIds);
                /**/
                logger.info("实际关闭订单:" + realCancelledOrderIds);

                for (String orderId : realCancelledOrderIds) {
                    saveOrderLog(orderId);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<String> cancelOrders(List<String> orderIds) {
        Query queryOrder = new Query();
        queryOrder.addCriteria(Criteria.where("orderId").in(orderIds));
        queryOrder.addCriteria(Criteria.where("status").is(TO_BE_CANCELLED_STATUS));

        List<ReqOrder> reqOrders = mongoOperationNew.find(queryOrder, ReqOrder.class);
        List<String> ret = new LinkedList<String>();
        for (ReqOrder reqOrder : reqOrders) {
            ret.add(reqOrder.getOrderId());
        }

        Update updateOrder = new Update();
        updateOrder.set("status", CANCELLED_STATUS);//变已关闭
        WriteResult writeResult = mongoOperationNew.updateMulti(queryOrder, updateOrder, ReqOrder.class);
        return ret;
    }

    public void saveOrderLog(String orderId) {
        ReqOrderLog log = new ReqOrderLog();
        log.setContent("订单因超时被关闭");
        log.setOperTime(System.currentTimeMillis());
        log.setOrderId(orderId);
        log.setOperAdminName("系统-定时任务");
        log.setOperAdminMobile("");
        log.setOperIp("0.0.0.0");
        mongoOperationNew.save(log);
    }

    private void markUpdatedDZFOrderIds(List<String> orderIds) {
        Query queryOrder = new Query();
        queryOrder.addCriteria(Criteria.where("orderId").in(orderIds));

        Update update = new Update();
        update.set("u", true);
        mongoOperationNew.updateMulti(queryOrder, update, DZFOrderIds.class);
    }
}
