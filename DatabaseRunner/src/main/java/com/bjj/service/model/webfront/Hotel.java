package com.bjj.service.model.webfront;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;

@Document(collection = "hotel")
public class Hotel implements Serializable {
    @Id
    private String id;
    @Field("name")
    private String text;

    @Override
    public String toString() {
        return "Hotel{" +
                "id='" + id + '\'' +
                ", text='" + text + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
    

}
