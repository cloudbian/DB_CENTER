package com.bjj.service.model.webfront;

import java.util.List;

/**
 * @author Jiangli
 *         <p/>
 *         CreatedTime  2015/3/27 0027 11:02
 * @Remark("没有对应表,只对应了ReqOrder.unit的数据模型")
 */
public class UnitPrice {
    private Double inputPrice;//单天输入单价
    private Long date;//价格对应日期
    private Double totalPrice;//单天计算出来的总价,=inputPrice*订单房间数
    List<ExtraService> extraService;


    public Double getInputPrice() {
        return inputPrice;
    }

    public void setInputPrice(Double inputPrice) {
        this.inputPrice = inputPrice;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

	public List<ExtraService> getExtraService() {
		return extraService;
	}

	public void setExtraService(List<ExtraService> extraService) {
		this.extraService = extraService;
	}
    
    
}
