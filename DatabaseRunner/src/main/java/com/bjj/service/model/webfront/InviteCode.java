/**
 * 
 */
package com.bjj.service.model.webfront;

import java.io.Serializable;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author June
 * CreateTime：2015-3-23  下午3:43:27
 */
@Document(collection="InviteCode")
public class InviteCode implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private ObjectId id;
	private String mobile;
	private String inviteCode;
	private int num;
	private long createTime;
	private List<String> childMobile;

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getInviteCode() {
		return inviteCode;
	}

	public void setInviteCode(String inviteCode) {
		this.inviteCode = inviteCode;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(long createTime) {
		this.createTime = createTime;
	}

	public List<String> getChildMobile() {
		return childMobile;
	}

	public void setChildMobile(List<String> childMobile) {
		this.childMobile = childMobile;
	}

	/**
	 * 
	 */
	public InviteCode() {
		super();
	}

	/**
	 * @param id
	 * @param mobile
	 * @param inviteCode
	 * @param num
	 * @param createTime
	 */
	@PersistenceConstructor
	public InviteCode(ObjectId id, String mobile, String inviteCode, int num, long createTime,List<String> childMobile) {
		super();
		this.id = id;
		this.mobile = mobile;
		this.inviteCode = inviteCode;
		this.num = num;
		this.createTime = createTime;
		this.childMobile = childMobile;
	}

}
