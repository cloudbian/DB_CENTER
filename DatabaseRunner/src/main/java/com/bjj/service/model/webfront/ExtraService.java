package com.bjj.service.model.webfront;

/**
 * @author Jiangli
 *         <p/>
 *         CreatedTime  2015/3/27 0027 11:02
 * @Remark("没有对应表,只对应了ReqOrder.extraService的数据模型")
 */
public class ExtraService {
    private Double inputPrice;//输入单价
    private String type;//类型
    private String remark;//备注

    public Double getInputPrice() {
        return inputPrice;
    }

    public void setInputPrice(Double inputPrice) {
        this.inputPrice = inputPrice;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "ExtraService{" +
                "inputPrice=" + inputPrice +
                ", type='" + type + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}
