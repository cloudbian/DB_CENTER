package com.bjj.service.run;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 */
public class Runner {
    private static final Log logger = LogFactory.getLog("Runner");

    /**
     * 关闭方法监控
     */
    private final static int SHUTDOWN_MONITOR = 0;
    /**
     * 监控全部方法
     */
    private final static int ALL_MONITOR = 1;

    /**
     * 监控部分方法
     */
    private final static int PART_MONITOR = 2;

    public static ApplicationContext c;

    /**
     * @param args
     */
    public static void main(String[] args) {

        logger.info("--------RMI Service is start ...------------");
        try {
            c = new ClassPathXmlApplicationContext("classpath:spring_config.xml");

        } catch (Throwable t) {
            logger.error("-------- RMI Service error ====> ", t);
        }
        logger.info("--------RMI Service is end ...------------");
    }


}
