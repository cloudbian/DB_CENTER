package com.bjj.service.domain;

import com.bjj.service.exception.MsgException;
import com.bjj.service.utils.CommonUtil;

import java.io.Serializable;

/**
 * @author Jiangli
 *         <p/>
 *         CreatedTime  2015/3/11 0011 13:50
 */
public class Msg<T> implements Serializable {
    private Integer code = 0;
    private String msg = "";
    private String msgDetail = "";
    private T data;

    public Msg() {
    }

    public Msg(T data) {
        this.data = data;
    }

    public Msg<T> error(int codeP, String msgP) {
        return error(codeP, msgP, null);
    }

    public Msg<T> error(int codeP, String msgP, String msgDetailP) {
        setCode(codeP);
        if (msgP != null) {
            setMsg(msgP);

        }
        if (msgDetailP != null) {
            setMsgDetail(msgDetailP);

        }
        return this;
    }

    public void throwException(int codeP, String msgP) throws MsgException {
        throwException(codeP, msgP, null);
    }

    public void throwException(int codeP) throws MsgException {
        throwException(codeP, null, null);
    }

    public void throwException(int codeP, String msgP, String msgDetailP) throws MsgException {
        error(codeP, msgP, msgDetailP);
        throw new MsgException();
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getMsgDetail() {
        return msgDetail;
    }

    public void setMsgDetail(String msgDetail) {
        this.msgDetail = msgDetail;
    }

    @Override
    public String toString() {
        return "Msg{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", msgDetail='" + msgDetail + '\'' +
                ", data=" + data +
                '}';
    }

    public Msg<T> throwIfNullException(Object obj, int codeP, String msgP) throws MsgException {
        if (obj == null) {
            throwException(codeP, msgP);
        }
        return this;
    }

    public Msg<T> throwIfStringNotObjectIdException(String obj, int codeP, String msgP) throws MsgException {
        if (!CommonUtil.isObjectId(obj)) {
            throwException(codeP, msgP);
        }
        return this;
    }
}
